# Android Firebase Friendly Chat Codelab

Este codalab es una versión resultante de [Firebase Android Codelab](https://codelabs.developers.google.com/codelabs/firebase-android/index.html)

En las diferentes 'branches' se encuentran los bloques de código necesarios para implementar las características y funciones que se agregan para usar múltiples características de Firebase.

- [Paso 1](https://gitlab.com/tonioguzmanf/android-firebase-chat/tree/01-firebase-setup#paso-1): Configuración Inicial
- [Paso 2](https://gitlab.com/tonioguzmanf/android-firebase-chat/tree/02-firebase-authentication#paso-2): Firebase Authentication
- [Paso 3](https://gitlab.com/tonioguzmanf/android-firebase-chat/tree/03-firebase-save-messages#paso-3): Firebase Realtime Database y Storage
- [Paso 4](https://gitlab.com/tonioguzmanf/android-firebase-chat/tree/04-firebase-invites#paso-4): Firebase Invites
- [Paso 5](https://gitlab.com/tonioguzmanf/android-firebase-chat/tree/05-firebase-analytics#paso-5): Firebase Anaytics
- [Paso 6](https://gitlab.com/tonioguzmanf/android-firebase-chat/tree/06-firebase-admob#paso-6): Firebase Admob
- [Paso 7](https://gitlab.com/tonioguzmanf/android-firebase-chat/tree/07-firebase-crash-reporting#paso-7): Firebase Crash Reporting
